set -x EDITOR /usr/bin/nvim
set -x VISUAL /usr/bin/nvim
set -x PAGER /usr/bin/less
set -x XDG_CONFIG_HOME $HOME/.config
set -x GOPATH $HOME/.go

set -g theme_color_scheme gruvbox

# disables stupid fish greeting
function fish_greeting
end

# required to disable annoying clock on the right
# in https://github.com/aneveux/theme-harleen theme
function fish_right_prompt
end

function gitforcepush
	git add .
	git commit -m "$argv"
	git push
end

alias nv=nvim
alias bat="bat --plain"
alias pr="poetry run"
alias pacman-unlock="sudo rm /var/lib/pacman/db.lck"
alias cp="cp -iv"
alias mv="mv -iv"
alias df="df -h"
alias du="du -sh"
alias ipp="curl ipinfo.io/ip"
alias swallow="devour"
alias c='clear'

# https://github.com/ogham/exa
#alias ls="exa --color=auto --group-directories-first"
#alias la="exa --color=auto -a --group-directories-first"
#alias ll="exa --color=auto -l --group-directories-first"
#alias tree="exa --color=auto -T -I __pycache__ --group-directories-first"
#alias l.="exa --color=auto -a | egrep '^\.'"

# startx on login
#if status is-login
    #if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        #echo "Hewwo, cutie! Starting X server..."
		#exec startx -- -keeptty

# enable python PEP582
if test -n "$PYTHONPATH"
    set -x PYTHONPATH '/home/q/.local/share/pdm/venv/lib/python3.10/site-packages/pdm/pep582' $PYTHONPATH
else
    set -x PYTHONPATH '/home/q/.local/share/pdm/venv/lib/python3.10/site-packages/pdm/pep582'
end

